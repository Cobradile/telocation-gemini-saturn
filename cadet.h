#pragma once
#define	CADET_SPEED	(8)
#define CADET_FALL_SPEED (9)
#define CADET_JUMP_SPEED (1)
#define CADET_JUMP_HEIGHT (7)
#define CADET_TURN_SPEED (5)
#include "character.h"

extern Character cadet;
extern State cadet_state;
void cadet_physics();
int setRot(int newRot);
void update_cadet();
void create_cadet();
void draw_cadet();