#pragma once
#define CAM_TURN_SPEED (5)

extern jo_camera           cam;
extern jo_pos3D_fixed      cam_pos;
extern jo_rot3D			cam_rot;
extern jo_fixed            desiredCamRot;

void set_cam_target(int tx, int ty, int tz);
void update_camera(void);