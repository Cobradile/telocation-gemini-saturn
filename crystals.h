#pragma once
#define CRYSTAL_COL (45)

typedef struct
{
    jo_pos3D_fixed pos;
    bool collected;
} Crystal;

extern Crystal crystals[3];

void create_crystals();
void draw_crystals();
void crystal_init(int index, int x, int y, int z);
void crystal_collect(int index);