#include <jo/jo.h>
#include "camera.h"

jo_camera           cam;
jo_pos3D_fixed      cam_pos;
jo_rot3D			cam_rot;
jo_fixed            desiredCamRot;

void set_cam_target(int tx, int ty, int tz)
{
    cam_pos.x = tx;
    cam_pos.y = ty;
    cam_pos.z = tz;
}

void update_camera(void)
{
    if (cam_rot.ry % 45 == 0)
    {
        if (jo_is_pad1_key_pressed(JO_KEY_L))
        {
            desiredCamRot -= 45;
            cam_rot.ry -= CAM_TURN_SPEED;
        }
        else if (jo_is_pad1_key_pressed(JO_KEY_R))
        {
            desiredCamRot += 45;
            cam_rot.ry += CAM_TURN_SPEED;
        }

        if (cam_rot.ry >= 360)
        {
            cam_rot.ry -= 360;
            desiredCamRot -= 360;
        }
        else if (cam_rot.ry < 0)
        {
            cam_rot.ry += 360;
            desiredCamRot += 360;
        }
    }
    else
    {
        if (cam_rot.ry < desiredCamRot) cam_rot.ry += CAM_TURN_SPEED;
        else if (cam_rot.ry > desiredCamRot) cam_rot.ry -= CAM_TURN_SPEED;
    }
}