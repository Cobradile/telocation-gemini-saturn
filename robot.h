#pragma once
#define	ROBOT_SPEED	(8)
#define ROBOT_FALL_SPEED (1)
#define ROBOT_TURN_SPEED (5)
#include "character.h"

extern Character robot;
extern State robot_state;

void create_robot();
void draw_robot();