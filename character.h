#pragma once

typedef enum
{
    IDLE,
    WALK,
    FALL,
    JUMP
} State;

typedef struct
{
    jo_pos3D_fixed pos, prevPos, lastStableLocation, velocity, col;
    jo_rot3D rot;
    bool can_jump, can_break, on_ground, currentlySelected;
    int cycle, desiredRot;
    State state;
} Character;