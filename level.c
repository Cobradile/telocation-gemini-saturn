#include <jo/jo.h>
#include "level.h"
#include "crystals.h"
#include "fox/foxgfx.h"

jo_vertice          plane_vertices[] = JO_3D_PLANE_VERTICES(CUBE_COL_XZ / 1.4);
jo_3d_quad          plane;
LevelElement        tile[NUM_TILES];
int                 current_tile;
jo_pos3D_fixed      groundPos, groundCol, cullCol;
PFXDATA platformMesh, wallMesh;

void                create_tile(ObjectType object, int nx, int nz)
{
    //int             i;

    // jo_3d_create_tile(tile_quads, tile_vertices);
    // for (i = 0; i < 6; ++i)
    //     jo_3d_set_texture(&tile_quads[i], 0);
    // tile_pos.x = JO_MULT_BY_65536(200);
    // tile_pos.y = -JO_MULT_BY_65536(90);
    // tile_pos.z = JO_MULT_BY_65536(60);
    // tile_col.x = JO_MULT_BY_65536(80);
    // tile_col.y = JO_MULT_BY_65536(155);
    // tile_col.z = JO_MULT_BY_65536(90);

    tile[current_tile].active = true;
    tile[current_tile].pos.x = JO_MULT_BY_65536(TILE_SIZE * nx);
    tile[current_tile].pos.z = JO_MULT_BY_65536(TILE_SIZE * nz);
    if (object == FLOOR)
    {
        tile[current_tile].pos.y = JO_MULT_BY_65536(CUBE_POS_Y);
        tile[current_tile].col.x = JO_MULT_BY_65536(CUBE_COL_XZ);
        tile[current_tile].col.y = JO_MULT_BY_65536(CUBE_COL_Y);
        //tile[current_tile].col.y = JO_MULT_BY_65536(100);
        tile[current_tile].col.z = JO_MULT_BY_65536(CUBE_COL_XZ);
    }
    if (object == CUBE)
    {
        tile[current_tile].pos.y = -JO_MULT_BY_65536(CUBE_POS_Y);
        tile[current_tile].col.x = JO_MULT_BY_65536(CUBE_COL_XZ);
        tile[current_tile].col.y = JO_MULT_BY_65536(CUBE_COL_Y);
        tile[current_tile].col.z = JO_MULT_BY_65536(CUBE_COL_XZ);
    }
    else if (object == WALL)
    {
        tile[current_tile].pos.y = -JO_MULT_BY_65536(WALL_POS_Y);
        tile[current_tile].col.x = JO_MULT_BY_65536(CUBE_COL_XZ);
        tile[current_tile].col.y = JO_MULT_BY_65536(WALL_COL_Y);
        tile[current_tile].col.z = JO_MULT_BY_65536(CUBE_COL_XZ);
    }
    tile[current_tile].type = object;

    if (current_tile < NUM_TILES)   current_tile++;

    // //AABB_Create_size(&box, jo_int2fixed(-37), jo_int2fixed(-29), jo_int2fixed(74), JO_FIXED_2);
}

void initialise_level(void)
{
    cullCol.x = JO_MULT_BY_65536(CULL_DISTANCE);
    cullCol.y = JO_MULT_BY_65536(CULL_DISTANCE);
    cullCol.z = JO_MULT_BY_65536(CULL_DISTANCE);
    for (int c = 0; c < NUM_TILES; c++)
    {
        tile[c].active = false;
        tile[c].pos.x = JO_MULT_BY_65536(999999);
        tile[c].pos.z = JO_MULT_BY_65536(999999);
    }

    LoadMeshFromSSV("PLATFORM.SSV",&platformMesh,0);
    jo_sprite_add_tga(JO_ROOT_DIR, "AFLOOR.TGA", JO_COLOR_White);
    jo_sprite_add_tga(JO_ROOT_DIR, "ALIWALL.TGA", JO_COLOR_White);
    jo_3d_create_plane(&plane, plane_vertices);
    jo_3d_set_texture(&plane, 0);
}

void                create_ghost(void)
{
    jo_3d_set_transparency(jo_3d_get_sprite_quad(1), true);
}

void                draw_ghost(void)
{
    jo_3d_push_matrix();
    jo_3d_draw_billboard(2,0,0,0);
    // static bool     sens = true;
    // static int      distance = -40;

    // jo_3d_translate_matrix_y(10);
    // jo_3d_translate_matrix_z(distance);
    // jo_3d_draw_sprite(2);

    // if (sens)
    // {
    //     --distance;
    //     if (distance < -55)
    //         sens ^= true;
    // }
    // else
    // {
    //     ++distance;
    //     if (distance > -40)
    //         sens ^= true;
    // }
    jo_3d_pop_matrix();
}

void                draw_floor(void)
{
    jo_3d_push_matrix();
    jo_3d_rotate_matrix_x(-90);
    jo_3d_translate_matrix_x(groundPos.x);
    jo_3d_translate_matrix_y(groundPos.y);
	jo_3d_translate_matrix_z(groundPos.z);
    jo_3d_draw(&plane);
    jo_3d_pop_matrix();
}

void                draw_tiles(void)
{
    for (int c = 0; c < NUM_TILES; c++)
    {
        if (tile[c].active)
        {
            jo_3d_push_matrix();
            jo_3d_translate_matrix_fixed(tile[c].pos.x, tile[c].pos.y, tile[c].pos.z);
            if (tile[c].type == FLOOR)
            {
                //FXDrawMeshStatic(&platformMesh,CL_Replace);
                jo_3d_rotate_matrix_x(-90);
                jo_3d_draw(&plane);
            }
            else if (tile[c].type == CUBE) 
            {
                jo_3d_translate_matrix_y(CUBE_POS_Y);
                FXDrawMeshStatic(&platformMesh,CL_Replace);
            }
            else if (tile[c].type == WALL) 
            {
                jo_3d_translate_matrix_y(CUBE_POS_Y);
                jo_3d_set_scale(1, 2, 1);
                FXDrawMeshStatic(&platformMesh,CL_Replace);
            }
            jo_3d_pop_matrix();
        }
    }
}

// jo_3d_mesh * loadLevel(const char *file, const char *dir)
// {
//     int face;
//     int model;
//     int point;
//     int coord;

//     unsigned char *stream = (unsigned char *)jo_fs_read_file_in_dir(file, dir, JO_NULL);
//     unsigned char *startAddress = stream;
// }