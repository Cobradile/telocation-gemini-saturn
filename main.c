/*
** Jo Sega Saturn Engine
** Copyright (c) 2012-2017, Johannes Fetz (johannesfetz@gmail.com)
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the Johannes Fetz nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL Johannes Fetz BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS CAM_) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <jo/jo.h>
#include "cadet.h"
#include "robot.h"
#include "physics.h"
#include "camera.h"
#include "level.h"

#include "fox/foxgfx.h"

int timer = 0;
bool loading = false;

typedef enum
{
    INTRO,
    MENU,
    GAME
} CurrentScene;

CurrentScene currentScene = -1;

PFXDATA URlogoMesh;
PFXDATA RocketMesh;
int logo_id;
int logoRot = 0;

static jo_palette bgPalette;
jo_palette *TgaPaletteHandling(void)
{
    jo_create_palette(&bgPalette);
    return (&bgPalette);
}

void			    my_draw(void)
{
    if (!loading)
    {
        jo_printf(1, 1, "*Telocation: Gemini Prototype*");
        jo_3d_camera_look_at(&cam);
        if (currentScene == INTRO)
        {
            jo_3d_push_matrix();
            jo_3d_rotate_matrix_x(30);
            jo_3d_translate_matrix_z(90);
            jo_3d_translate_matrix_y(30);
            jo_3d_rotate_matrix_y(logoRot);
            FXDrawMeshStatic(&URlogoMesh,CL_Replace);
            jo_3d_pop_matrix();
            jo_printf(3, 25, "Press start");
        }
        else if (currentScene == MENU)
        {
            jo_3d_push_matrix();
            jo_3d_translate_matrix_x(-50);
            jo_3d_translate_matrix_y(10);
            jo_3d_rotate_matrix_z(90);
            jo_3d_set_scale(15, 15, 15);
            FXDrawMeshStatic(&RocketMesh,CL_Replace);
            jo_3d_pop_matrix();
            jo_sprite_draw3D(logo_id, 0, -60, 600);
            jo_printf(3, 25, "Press A to start");
        }
        else if (currentScene == GAME)
        {
            jo_printf(3, 25, "                  ");
            jo_3d_rotate_matrix_x(45);
            jo_3d_rotate_matrix_y(cam_rot.ry);
            jo_3d_translate_matrix_fixed(cam_pos.x, cam_pos.y, cam_pos.z);

            //draw_ghost();
            draw_tiles();

            draw_cadet();
            //draw_robot();
            draw_crystals();
        }
    }
    else
    {
        //jo_printf(10, 15, "LOADING");
        jo_sprite_change_sprite_scale(2);
        jo_sprite_draw3D(logo_id, 0, 0, 600);
        jo_sprite_restore_sprite_scale();
    }
}

void load_next_scene(CurrentScene newScene)
{
    jo_clear_background(JO_COLOR_Black);
    jo_clear_screen();
    currentScene = newScene;
    loading = true;
    ResetModelHeap();
    jo_sprite_free_from(0);
    jo_audio_stop_cd();
    logo_id = jo_sprite_add_tga(JO_ROOT_DIR, "LOAD.TGA", JO_COLOR_Green);
}

void update(void)
{
    if (loading)
    {
        if (timer >= 100)
        {
            timer = 0;
            set_scene(currentScene);
        }
        if (timer >= 100)
        {
            timer = 0;
            set_scene(currentScene);
        }
        else
        {
            timer++;
        }
    }
    else
    {
        if (currentScene == INTRO)
        {
            if (logoRot > 0) logoRot -= 1;
            else logoRot = 360;
            if (jo_is_pad1_key_pressed(JO_KEY_START))
            {
                load_next_scene(MENU);
            }
        }
        else if (currentScene == MENU)
        {
            if (jo_is_pad1_key_pressed(JO_KEY_A))
            {
                load_next_scene(GAME);
            }
        }
        else if (currentScene == GAME)
        {
            if (timer <= 0)
            {
                if (jo_is_pad1_key_pressed(JO_KEY_C))
                {
                        if (cadet.currentlySelected)
                        {
                            cadet.currentlySelected = false;
                            robot.currentlySelected = true;
                        }
                        else if (robot.currentlySelected)
                        {
                            cadet.currentlySelected = true;
                            robot.currentlySelected = false;
                        }
                        timer = 2;
                }
            }
            else 
            {
                if (!jo_is_pad1_key_pressed(JO_KEY_C)) timer -= 1;
            }

            update_camera();
            update_cadet();
            //update_robot();
        }
    }
}

void gradient_bg()
{
    Uint16 *backColTable = (Uint16 *) (VDP2_VRAM_B0 - JO_TV_HEIGHT * 2);
    
    int Ra = 1, Ga = 1, Ba = 25, Rb = 15, Gb = 4, Bb = 3;
    int Rf = Ra, Gf = Ga, Bf = Ba;
    float t = 0.0f;

    for (int i = 0; i < JO_TV_HEIGHT; i++) {
        Rf = lerp(Ra, Rb, t);
        Gf = lerp(Ga, Gb, t);
        Bf = lerp(Ba, Bb, t);
        t += 1.0f / JO_TV_HEIGHT;
        backColTable[i] = C_RGB(Rf, Gf, Bf);
    }
    
    slBackColTable((void *) backColTable);
}

void set_scene(CurrentScene scene)
{
    ResetModelHeap();
    jo_sprite_free_from(0);
    //jo_audio_stop_cd();
    jo_clear_background(JO_COLOR_Black);
    jo_clear_screen();
    if (scene == GAME)
    {
        jo_clear_background(JO_COLOR_Transparent);
        //jo_3d_free_mesh(&Meshspinninglogo);
        //create_ghost();
        //jo_set_background_sprite();
            gradient_bg();
            //create_plane();

            initialise_level();
            create_cadet();
            //create_robot();
            create_crystals();
            
            // for (int x = 0; x < 50; x++)
            // {
            //     for (int y = 0; y < 50; y++)

            crystal_init(0, 6, -CUBE_COL_Y, 4);
            crystal_init(1, 2, -CUBE_COL_Y, 10);
            crystal_init(2, 1, 20, 17);
            
            create_tile(CUBE, -1, 0);
            create_tile(FLOOR, 0, 0);
            create_tile(FLOOR, 1, 0);
            create_tile(FLOOR, 2, 0);
            create_tile(CUBE, 3, 0);

            create_tile(CUBE, -1, 1);
            create_tile(FLOOR, 0, 1);
            create_tile(FLOOR, 1, 1);
            create_tile(FLOOR, 2, 1);
            create_tile(CUBE, 3, 1);

            create_tile(CUBE, -1, 2);
            create_tile(FLOOR, 0, 2);
            create_tile(FLOOR, 1, 2);
            create_tile(FLOOR, 2, 2);
            create_tile(CUBE, 3, 2);            
            
            create_tile(CUBE, -1, 3);
            create_tile(CUBE, 0, 3);
            create_tile(FLOOR, 1, 3);
            create_tile(CUBE, 2, 3);
            create_tile(CUBE, 3, 3);
            create_tile(CUBE, 4, 3);
            create_tile(CUBE, 5, 3);
            create_tile(CUBE, 6, 3);
            create_tile(CUBE, 7, 3);
            create_tile(CUBE, 8, 3);
            create_tile(CUBE, 9, 3);
            create_tile(WALL, 10, 3);

            create_tile(WALL, 0, 4);
            create_tile(FLOOR, 1, 4);
            create_tile(FLOOR, 2, 4);
            create_tile(FLOOR, 3, 4);
            create_tile(FLOOR, 4, 4);
            create_tile(FLOOR, 5, 4);
            create_tile(CUBE, 6, 4);
            create_tile(FLOOR, 7, 4);
            create_tile(FLOOR, 8, 4);
            create_tile(FLOOR, 9, 4);
            create_tile(WALL, 10, 4);

            create_tile(WALL, 0, 5);
            create_tile(FLOOR, 1, 5);
            create_tile(FLOOR, 2, 5);
            create_tile(FLOOR, 3, 5);
            create_tile(FLOOR, 4, 5);
            create_tile(FLOOR, 5, 5);
            create_tile(CUBE, 6, 5);
            create_tile(FLOOR, 7, 5);
            create_tile(FLOOR, 8, 5);
            create_tile(FLOOR, 9, 5);
            create_tile(WALL, 10, 5);

            create_tile(WALL, 0, 6);
            create_tile(WALL, 1, 6);
            create_tile(WALL, 2, 6);
            create_tile(WALL, 3, 6);
            create_tile(WALL, 4, 6);
            create_tile(WALL, 5, 6);
            create_tile(WALL, 6, 6);
            create_tile(WALL, 7, 6);
            create_tile(WALL, 8, 6);
            create_tile(FLOOR, 9, 6);
            create_tile(WALL, 10, 6);

            create_tile(WALL, 0, 7);
            create_tile(FLOOR, 1, 7);
            create_tile(FLOOR, 2, 7);
            create_tile(FLOOR, 3, 7);
            create_tile(FLOOR, 4, 7);
            create_tile(FLOOR, 5, 7);
            create_tile(FLOOR, 6, 7);
            create_tile(FLOOR, 7, 7);
            create_tile(FLOOR, 8, 7);
            create_tile(FLOOR, 9, 7);
            create_tile(WALL, 10, 7);

            create_tile(WALL, 0, 8);
            create_tile(FLOOR, 1, 8);
            create_tile(CUBE, 2, 8);
            create_tile(FLOOR, 3, 8);
            create_tile(FLOOR, 4, 8);
            create_tile(FLOOR, 5, 8);
            create_tile(CUBE, 6, 8);
            create_tile(FLOOR, 7, 8);
            create_tile(FLOOR, 8, 8);
            create_tile(FLOOR, 9, 8);
            create_tile(WALL, 10, 8);

            create_tile(WALL, 0, 9);
            create_tile(FLOOR, 1, 9);
            create_tile(FLOOR, 2, 9);
            create_tile(FLOOR, 3, 9);
            create_tile(FLOOR, 4, 9);
            create_tile(FLOOR, 5, 9);
            create_tile(FLOOR, 6, 9);
            create_tile(FLOOR, 7, 9);
            create_tile(FLOOR, 8, 9);
            create_tile(FLOOR, 9, 9);
            create_tile(WALL, 10, 9);

            create_tile(WALL, 0, 10);
            create_tile(FLOOR, 1, 10);
            create_tile(CUBE, 2, 10);
            create_tile(FLOOR, 3, 10);
            create_tile(FLOOR, 4, 10);
            create_tile(FLOOR, 5, 10);
            create_tile(CUBE, 6, 10);
            create_tile(FLOOR, 7, 10);
            create_tile(FLOOR, 8, 10);
            create_tile(FLOOR, 9, 10);
            create_tile(WALL, 10, 10);

            create_tile(WALL, 0, 11);
            create_tile(FLOOR, 1, 11);
            create_tile(FLOOR, 2, 11);
            create_tile(FLOOR, 3, 11);
            create_tile(FLOOR, 4, 11);
            create_tile(FLOOR, 5, 11);
            create_tile(FLOOR, 6, 11);
            create_tile(FLOOR, 7, 11);
            create_tile(FLOOR, 8, 11);
            create_tile(FLOOR, 9, 11);
            create_tile(WALL, 10, 11);

            create_tile(WALL, 0, 12);
            create_tile(CUBE, 1, 12);
            create_tile(WALL, 2, 12);
            create_tile(WALL, 3, 12);
            create_tile(WALL, 4, 12);
            create_tile(WALL, 5, 12);
            create_tile(WALL, 6, 12);
            create_tile(WALL, 7, 12);
            create_tile(WALL, 8, 12);
            create_tile(WALL, 9, 12);
            create_tile(WALL, 10, 12);

            create_tile(FLOOR, 1, 16);
            create_tile(FLOOR, 1, 17);
            create_tile(FLOOR, 2, 17);

            create_tile(CUBE, 4, 17);
            create_tile(CUBE, 4, 18);
            create_tile(CUBE, 5, 17);
            create_tile(CUBE, 5, 18);

            create_tile(FLOOR, 8, 17);
            create_tile(FLOOR, 8, 18);
            create_tile(FLOOR, 9, 17);
            create_tile(FLOOR, 9, 18);

            jo_audio_play_cd_track(2, 2, 1);
        }
        else if (scene == INTRO)
        {
            LoadMeshFromSSV("URLOGO.SSV",&URlogoMesh,0);
            jo_sprite_add_tga(JO_ROOT_DIR, "YELLOW.TGA", JO_COLOR_White);
            jo_sprite_add_tga(JO_ROOT_DIR, "RED.TGA", JO_COLOR_White);
            jo_sprite_add_tga(JO_ROOT_DIR, "GREEN.TGA", JO_COLOR_White);
            jo_sprite_add_tga(JO_ROOT_DIR, "BLUE.TGA", JO_COLOR_White);
            jo_audio_play_cd_track(3, 3, 0);
        }
        else if (scene == MENU)
        {
            logo_id = jo_sprite_add_tga(JO_ROOT_DIR, "TITLE.TGA", JO_COLOR_Green);
            LoadMeshFromSSV("ROCKET.SSV",&RocketMesh,1);
            jo_sprite_add_tga(JO_ROOT_DIR, "GRAY.TGA", JO_COLOR_White);
            jo_audio_play_cd_track(4, 4, 1);
        }
    currentScene = scene;
    loading = false;
}

void			jo_main(void)
{
	jo_core_init(JO_COLOR_Black);

    jo_3d_camera_init(&cam);

    //jo_sprite_add_tga(JO_ROOT_DIR, "G.TGA", JO_COLOR_Green);

    set_scene(GAME);

    jo_core_add_callback(update);
	jo_core_add_callback(my_draw);
	jo_core_run();
}

/*
** END OF FILE
*/
