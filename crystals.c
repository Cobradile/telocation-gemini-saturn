#include <jo/jo.h>
#include "crystals.h"
#include "level.h"
#include "fox/foxgfx.h"

Crystal crystals[3];
jo_fixed rot = 0;
PFXDATA crystalsMesh;

void create_crystals()
{
    LoadMeshFromSSV("CRYSTALS.SSV",&crystalsMesh,4);
    jo_sprite_add_tga(JO_ROOT_DIR, "GREEN.TGA", JO_COLOR_White);
}

void draw_crystals()
{
    if (rot < 360) rot++;
    else rot = 0;

    for (int c = 0; c < 3; c++)
    {
        if (!crystals[c].collected)
        {
            jo_3d_push_matrix();
            jo_3d_translate_matrix_fixed(crystals[c].pos.x, crystals[c].pos.y, crystals[c].pos.z);
            jo_3d_rotate_matrix_y(rot);
            jo_3d_translate_matrix_y(86);
            jo_3d_set_scale(4, 4, 4);
            FXDrawMeshStatic(&crystalsMesh,CL_Replace);
            jo_3d_pop_matrix();
        }
    }
}

void crystal_init(int index, int x, int y, int z)
{
    crystals[index].collected = false;
    crystals[index].pos.x = JO_MULT_BY_65536(TILE_SIZE * x);
    crystals[index].pos.y = JO_MULT_BY_65536(y - 86);
    crystals[index].pos.z = JO_MULT_BY_65536(TILE_SIZE * z);
}

void crystal_collect(int index)
{
    crystals[index].collected = true;
}