#pragma once
#define GRAVITY (9)

int lerp(int a, int b, float f);
int distance_between(int x1, int y1, int x2, int y2);
bool do_collision(
    jo_pos3D_fixed prevPos1,
    jo_pos3D_fixed pos1,
    jo_pos3D_fixed col1,
    jo_pos3D_fixed pos2,
    const jo_pos3D_fixed col2,
    jo_vector_fixed * result);
bool is_in_box(
    const jo_pos3D_fixed * pointCur,
    const jo_fixed pointCol,
    const jo_pos3D_fixed * aabbCenter,
    const jo_pos3D_fixed * aabbRadius);
bool CollisionCheckY(
    const jo_pos3D_fixed * pointPrev,
    const jo_pos3D_fixed * pointCur,
    const jo_pos3D_fixed * pointCol,
    const jo_pos3D_fixed * aabbCenter,
    const jo_pos3D_fixed * aabbRadius,
    jo_vector_fixed * repulsionVector);
bool do_item_collision(
    const jo_pos3D_fixed * pointPrev,
    const jo_pos3D_fixed * pointCur,
    const jo_pos3D_fixed * pointCol,
    const jo_pos3D_fixed * itemPos,
    const jo_fixed itemCol);