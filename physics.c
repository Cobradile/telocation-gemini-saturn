#include <jo/jo.h>
#include "physics.h"

int lerp(int a, int b, float f)
{
    return a + f * (b - a);
}

bool SweptCollisionPointToAABB(
    const jo_pos3D_fixed * pointPrev,
    const jo_pos3D_fixed * pointCur,
    const jo_pos3D_fixed * pointCol,
    const jo_pos3D_fixed * aabbCenter,
    const jo_pos3D_fixed * aabbRadius,
    jo_vector_fixed * repulsionVector)
{
    jo_fixed entryTime;
    jo_fixed entryX;
    jo_fixed entryY;
    jo_fixed entryZ;
    bool collisionX = false;
    bool collisionY = false;
    bool collisionZ = false;

    jo_vector_fixed movementVector = { { pointCur->x - pointPrev->x, pointCur->y - pointPrev->y, pointCur->z - pointPrev->z } };

    // Default to maximal entry time (1.0f)
    repulsionVector->x = JO_FIXED_1;
    repulsionVector->y = JO_FIXED_1;
    repulsionVector->z = JO_FIXED_1;

    // Previous Player sides
    jo_fixed prev_right_a = pointPrev->x - pointCol->x;
    jo_fixed prev_left_a = pointPrev->x + pointCol->x;

    jo_fixed prev_forward_a = pointPrev->y + pointCol->y;
    jo_fixed prev_backward_a = pointPrev->y - pointCol->y;

    jo_fixed prev_top_a = pointPrev->z - pointCol->z;
    jo_fixed prev_bottom_a = pointPrev->z + pointCol->z;

    // Player sides
    jo_fixed right_a = pointCur->x - pointCol->x;
    jo_fixed left_a = pointCur->x + pointCol->x;

    jo_fixed forward_a = pointCur->y + pointCol->y;
    jo_fixed backward_a = pointCur->y - pointCol->y;

    jo_fixed top_a = pointCur->z - pointCol->z;
    jo_fixed bottom_a = pointCur->z + pointCol->z;

    // Box sides
    jo_fixed right = aabbCenter->x + aabbRadius->x;
    jo_fixed left = aabbCenter->x - aabbRadius->x;

    jo_fixed forward = aabbCenter->y + aabbRadius->y;
    jo_fixed backward = aabbCenter->y - aabbRadius->y;

    jo_fixed top = aabbCenter->z + aabbRadius->z;
    jo_fixed bottom = aabbCenter->z - aabbRadius->z;

    //jo_printf(0, 27, "PlayerX: %d PrevX: %d", pointCur->x, pointPrev->x);
    //jo_printf(0, 28, "CubeX: %d CubeW: %d", -aabbCenter->x, right);

    // Check for collision on X axis line
    if (movementVector.x != 0)
    {
        if ((movementVector.x > 0 && prev_left_a <= left && left_a > left) ||
             (movementVector.x < 0 && prev_right_a >= right && right_a < right))
        {
            // We got collision
            if (movementVector.x > 0)
            {
                entryTime = left - prev_left_a;
            }
            else
            {
                entryTime = right - prev_right_a;
            }

            // Calculate entry time (distance from previous position to the box along movement direction)
            entryTime = jo_fixed_div(entryTime, movementVector.x);
            entryY = jo_fixed_mult(movementVector.y, entryTime);
            entryZ = jo_fixed_mult(movementVector.z, entryTime);

            // Check for Y and Z collision
            if (prev_forward_a + entryY <= forward && 
                prev_backward_a + entryY >= backward &&
                prev_top_a + entryZ <= top && 
                prev_bottom_a + entryZ >= bottom)
            {
                repulsionVector->x = entryTime;
                collisionX = true;
            }
        }
    }

    // Check for collision on Y axis line
    if (movementVector.y != 0)
    {
        if ((movementVector.y > 0 && prev_backward_a <= backward && backward_a > backward) ||
            (movementVector.y < 0 && prev_forward_a >= forward && forward_a < forward))
        {
            // We got collision
            if (movementVector.y > 0)
            {
                entryTime = backward - prev_backward_a;
            }
            else
            {
                entryTime = forward - prev_forward_a;
            }

            // Calculate entry time (distance from previous position to the box along movement direction)
            entryTime = jo_fixed_div(entryTime, movementVector.y);
            entryX = jo_fixed_mult(movementVector.x, entryTime);
            entryZ = jo_fixed_mult(movementVector.z, entryTime);

            // Check for X and Z collision
            if (prev_right_a + entryX <= right &&
                prev_left_a + entryX >= left &&
                prev_top_a + entryZ <= top && 
                prev_bottom_a + entryZ >= bottom)
            {
                repulsionVector->y = entryTime;
                collisionY = true;
            }
        }
    }
        
    // Check for collision on Z axis line
    if (movementVector.z != 0)
    {
        if ((movementVector.z > 0 && prev_bottom_a <= bottom && bottom_a > bottom) ||
            (movementVector.z < 0 && prev_top_a >= top && top_a < top))
        {
            // We got collision
            if (movementVector.z > 0)
            {
                entryTime = bottom - prev_bottom_a;
            }
            else
            {
                entryTime = top - prev_top_a;
            }

            // Calculate entry time (distance from previous position to the box along movement direction)
            entryTime = jo_fixed_div(entryTime, movementVector.z);
            entryX = jo_fixed_mult(movementVector.x, entryTime);
            entryY = jo_fixed_mult(movementVector.y, entryTime);

            // Check for X and Y collision
            if (prev_right_a + entryX <= right &&
                prev_left_a + entryX >= left &&
                prev_forward_a + entryY <= forward && 
                prev_backward_a + entryY >= backward)
            {
                repulsionVector->z = entryTime;
                collisionZ = true;
            }
        }
    }

    return collisionX || collisionY || collisionZ;
}

int distance_between(int x1, int y1, int x2, int y2)
{
    return JO_ABS(x1 - x2) + JO_ABS(y1 - y2);
}

bool is_in_box(
    const jo_pos3D_fixed * pointCur,
    const jo_fixed pointCol,
    const jo_pos3D_fixed * aabbCenter,
    const jo_pos3D_fixed * aabbRadius)
{
    // Player sides
    jo_fixed right_a = pointCur->x - pointCol;
    jo_fixed left_a = pointCur->x + pointCol;

    jo_fixed forward_a = pointCur->y + pointCol;
    jo_fixed backward_a = pointCur->y - pointCol;

    jo_fixed top_a = pointCur->z - pointCol;
    jo_fixed bottom_a = pointCur->z + pointCol;

    // Box sides
    jo_fixed right = aabbCenter->x + aabbRadius->x;
    jo_fixed left = aabbCenter->x - aabbRadius->x;

    jo_fixed forward = aabbCenter->y + aabbRadius->y;
    jo_fixed backward = aabbCenter->y - aabbRadius->y;

    jo_fixed top = aabbCenter->z + aabbRadius->z;
    jo_fixed bottom = aabbCenter->z - aabbRadius->z;

    return left_a < right &&
           right_a > left &&
           bottom_a < top &&
           top_a > bottom;
}

bool do_collision(
    jo_pos3D_fixed prevPos1,
    jo_pos3D_fixed pos1,
    jo_pos3D_fixed col1,
    jo_pos3D_fixed pos2,
    const jo_pos3D_fixed col2,
    jo_vector_fixed * result)
{
    jo_printf(0, 28, "Pos1: %d Pos2: %d", pos1.x, pos2.x);
    // Check whether our movement vector is passing boundry of AABB
    if (SweptCollisionPointToAABB(&prevPos1, &pos1, &col1, &pos2, &col2, &result))
    {
        // Swept collision response
        pos1.x = prevPos1.x + jo_fixed_mult(pos1.x - prevPos1.x, result->x);
        pos1.z = prevPos1.z + jo_fixed_mult(pos1.z - prevPos1.z, result->z);
        pos1.y = prevPos1.y + jo_fixed_mult(pos1.y - prevPos1.y, result->y);
        return true;
    }
    jo_printf(0, 29, "Pos1: %d Pos2: %d", pos1.x, pos2.x);
    return false;
}

bool do_item_collision(
    const jo_pos3D_fixed * pointPrev,
    const jo_pos3D_fixed * pointCur,
    const jo_pos3D_fixed * pointCol,
    const jo_pos3D_fixed * itemPos,
    const jo_fixed itemCol)
{
    jo_fixed entryTime;
    jo_fixed entryX;
    jo_fixed entryY;
    jo_fixed entryZ;
    bool collisionX = false;
    bool collisionY = false;
    bool collisionZ = false;

    jo_vector_fixed movementVector = { { pointCur->x - pointPrev->x, pointCur->y - pointPrev->y, pointCur->z - pointPrev->z } };

    // Previous Player sides
    jo_fixed prev_right_a = pointPrev->x - pointCol->x;
    jo_fixed prev_left_a = pointPrev->x + pointCol->x;

    jo_fixed prev_forward_a = pointPrev->y + pointCol->y;
    jo_fixed prev_backward_a = pointPrev->y - pointCol->y;

    jo_fixed prev_top_a = pointPrev->z - pointCol->z;
    jo_fixed prev_bottom_a = pointPrev->z + pointCol->z;

    // Player sides
    jo_fixed right_a = pointCur->x - pointCol->x;
    jo_fixed left_a = pointCur->x + pointCol->x;

    jo_fixed forward_a = pointCur->y + pointCol->y;
    jo_fixed backward_a = pointCur->y - pointCol->y;

    jo_fixed top_a = pointCur->z - pointCol->z;
    jo_fixed bottom_a = pointCur->z + pointCol->z;

    // Box sides
    jo_fixed right = itemPos->x + itemCol;
    jo_fixed left = itemPos->x - itemCol;

    jo_fixed forward = itemPos->y + itemCol;
    jo_fixed backward = itemPos->y - itemCol;

    jo_fixed top = itemPos->z + itemCol;
    jo_fixed bottom = itemPos->z - itemCol;

    //jo_printf(0, 27, "PlayerX: %d PrevX: %d", pointCur->x, pointPrev->x);
    //jo_printf(0, 28, "CubeX: %d CubeW: %d", -aabbCenter->x, right);

    // Check for collision on X axis line
    if (movementVector.x != 0)
    {
        if ((movementVector.x > 0 && prev_left_a <= left && left_a > left) ||
             (movementVector.x < 0 && prev_right_a >= right && right_a < right))
        {
            // Check for Y and Z collision
            if (prev_forward_a + entryY <= forward && 
                prev_backward_a + entryY >= backward &&
                prev_top_a + entryZ <= top && 
                prev_bottom_a + entryZ >= bottom)
            {
                collisionX = true;
            }
        }
    }

    // Check for collision on Y axis line
    if (movementVector.y != 0)
    {
        if ((movementVector.y > 0 && prev_backward_a <= backward && backward_a > backward) ||
            (movementVector.y < 0 && prev_forward_a >= forward && forward_a < forward))
        {
            // Check for X and Z collision
            if (prev_right_a + entryX <= right &&
                prev_left_a + entryX >= left &&
                prev_top_a + entryZ <= top && 
                prev_bottom_a + entryZ >= bottom)
            {
                collisionY = true;
            }
        }
    }
        
    // Check for collision on Z axis line
    if (movementVector.z != 0)
    {
        if ((movementVector.z > 0 && prev_bottom_a <= bottom && bottom_a > bottom) ||
            (movementVector.z < 0 && prev_top_a >= top && top_a < top))
        {
            // Check for X and Y collision
            if (prev_right_a + entryX <= right &&
                prev_left_a + entryX >= left &&
                prev_forward_a + entryY <= forward && 
                prev_backward_a + entryY >= backward)
            {
                collisionZ = true;
            }
        }
    }

    return collisionX || collisionY || collisionZ;
}

bool CollisionCheckY(
    const jo_pos3D_fixed * pointPrev,
    const jo_pos3D_fixed * pointCur,
    const jo_pos3D_fixed * pointCol,
    const jo_pos3D_fixed * aabbCenter,
    const jo_pos3D_fixed * aabbRadius,
    jo_vector_fixed * repulsionVector)
{
    jo_fixed entryTime;
    jo_fixed entryY;
    bool collisionY = false;

    jo_vector_fixed movementVector = { { pointCur->x - pointPrev->x, pointCur->y - pointPrev->y, pointCur->z - pointPrev->z } };

    // Default to maximal entry time (1.0f)
    repulsionVector->y = JO_FIXED_1;

    // Previous Player sides
    jo_fixed prev_forward_a = pointPrev->y + pointCol->y;
    jo_fixed prev_backward_a = pointPrev->y - pointCol->y;

    // Player sides
    jo_fixed forward_a = pointCur->y + pointCol->y;
    jo_fixed backward_a = pointCur->y - pointCol->y;

    // Box sides
    jo_fixed forward = -aabbCenter->y + aabbRadius->y;
    jo_fixed backward = -aabbCenter->y - aabbRadius->y;

    //jo_printf(0, 27, "PlayerX: %d PrevX: %d", pointCur->x, pointPrev->x);
    //jo_printf(0, 28, "CubeX: %d CubeW: %d", -aabbCenter->x, right);

    // Check for collision on Y axis line
    if (movementVector.y != 0)
    {
        if ((movementVector.y > 0 && prev_backward_a <= backward && backward_a > backward) ||
            (movementVector.y < 0 && prev_forward_a >= forward && forward_a < forward))
        {
            // We got collision
            if (movementVector.y > 0)
            {
                entryTime = backward - prev_backward_a;
            }
            else
            {
                entryTime = forward - prev_forward_a;
            }

            // Calculate entry time (distance from previous position to the box along movement direction)
            entryTime = jo_fixed_div(entryTime, movementVector.y);
            
            repulsionVector->y = entryTime;
            collisionY = true;
        }
    }
    else
    {
        collisionY = true;
    }

    return collisionY;
}