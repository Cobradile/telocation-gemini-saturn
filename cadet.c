#include <jo/jo.h>
#include "cadet.h"
#include "models/cadet.h"
#include "physics.h"
#include "camera.h"
#include "level.h"
#include "crystals.h"

Character cadet;
State cadet_state;

void cadet_physics()
{
    //jo_printf(0, 25, "                  ");
    //jo_printf(0, 26, "  ");
    jo_vector_fixed result;

    if (cadet.on_ground)
    {
        if (cadet_state != WALK) cadet_state = IDLE;
    }
    else if (cadet_state == JUMP)
    {
        if (cadet.velocity.y <= JO_MULT_BY_65536(CADET_JUMP_HEIGHT)) cadet.velocity.y += JO_MULT_BY_65536(CADET_JUMP_SPEED) / 10;
        else cadet_state = FALL;
    }
    else if (cadet.velocity.y > -JO_MULT_BY_65536(CADET_FALL_SPEED))
    {
        cadet.velocity.y -= JO_MULT_BY_65536(GRAVITY) / 20;
    }
    //cadet.prevPos.y = cadet.pos.y;
    cadet.pos.y -= cadet.velocity.y;
    if (ABS(cadet.velocity.y) < 0.1) cadet.lastStableLocation = cadet.pos;
    else if (cadet.pos.y > JO_MULT_BY_65536(300)) cadet.pos = cadet.lastStableLocation;
    
    // // Check whether our movement vector is passing boundry of AABB
    // if (SweptCollisionPointToAABB(&cadet.prevPos, &cadet.pos, &cadet.col, &groundPos, &groundCol, &result))
    // {
    //     // Swept collision response
    //     cadet.pos.x = cadet.prevPos.x + jo_fixed_mult(cadet.pos.x - cadet.prevPos.x, result.x);
    //     cadet.pos.z = cadet.prevPos.z + jo_fixed_mult(cadet.pos.z - cadet.prevPos.z, result.z);
    //     cadet.pos.y = cadet.prevPos.y + jo_fixed_mult(cadet.pos.y - cadet.prevPos.y, result.y);
    // }

    for (int c = 0; c < NUM_TILES; c++)
    {
        if (distance_between(cadet.pos.x, cadet.pos.z, tile[c].pos.x, tile[c].pos.z) < JO_MULT_BY_65536(800)) tile[c].active = true;
        else tile[c].active = false;
    }

    for (int c = 0; c < NUM_TILES; c++)
    {
        if (&tile[c].active)
        {
            if (SweptCollisionPointToAABB(&cadet.prevPos, &cadet.pos, &cadet.col, &tile[c].pos, &tile[c].col, &result))
            {
                if (&tile[c].type != FLOOR)
                {
                    cadet.pos.x = cadet.prevPos.x + jo_fixed_mult(cadet.pos.x - cadet.prevPos.x, result.x);
                    cadet.pos.z = cadet.prevPos.z + jo_fixed_mult(cadet.pos.z - cadet.prevPos.z, result.z);
                }
                cadet.pos.y = cadet.prevPos.y + jo_fixed_mult(cadet.pos.y - cadet.prevPos.y, result.y);
            }
        }
    }  

    for (int c = 0; c < 3; c++)
    {
        
        if (!crystals[c].collected)
        {
            if (do_item_collision(&cadet.prevPos, &cadet.pos, &cadet.col, &crystals[c].pos, JO_MULT_BY_65536(CRYSTAL_COL)))
            {
                crystal_collect(c);
            }
        }
    }  

    cadet.on_ground = CollisionCheckY(&cadet.prevPos, &cadet.pos, &cadet.col, &groundPos, &groundCol, &result);

    cadet.prevPos = cadet.pos;
}

int setRot(int newRot)
{
    if (newRot >= 360) newRot -= 360;
    else if (newRot < 0) newRot += 360;
    return newRot;
}

void update_cadet()
{
    cadet_physics();
    if (cadet.currentlySelected)
    {
        if (jo_is_pad1_key_pressed(JO_KEY_UP) || jo_is_pad1_key_pressed(JO_KEY_DOWN) || jo_is_pad1_key_pressed(JO_KEY_LEFT) || jo_is_pad1_key_pressed(JO_KEY_RIGHT))
        {
            if (cadet.on_ground)
            {
                cadet_state = WALK;
                cadet.cycle -= 5;
            }

            if (cadet.rot.ry < cadet.desiredRot) cadet.rot.ry +=  CADET_TURN_SPEED;
            else if (cadet.rot.ry > cadet.desiredRot) cadet.rot.ry -=  CADET_TURN_SPEED;

            // Check whether our movement vector is passing boundry of AABB
            if (jo_is_pad1_key_pressed(JO_KEY_UP))
            {
                cadet.desiredRot = setRot(desiredCamRot + 180);
                cadet.pos.x -= jo_sin(cam_rot.ry) * CADET_SPEED;
                cadet.pos.z += jo_cos(cam_rot.ry) * CADET_SPEED;
            }
            else if (jo_is_pad1_key_pressed(JO_KEY_DOWN))
            {
                cadet.desiredRot = setRot(desiredCamRot);
                cadet.pos.x += jo_sin(cam_rot.ry) * CADET_SPEED;
                cadet.pos.z -= jo_cos(cam_rot.ry) * CADET_SPEED;
            }

            if (cadet.cycle >= 180) cadet.cycle = 0;
            else if (cadet.cycle <= 0) cadet.cycle = 180;
            
            if (jo_is_pad1_key_pressed(JO_KEY_LEFT))
            {
                cadet.desiredRot = setRot(desiredCamRot - 90);
                cadet.pos.x -= jo_cos(cam_rot.ry) * CADET_SPEED;
                cadet.pos.z -= jo_sin(cam_rot.ry) * CADET_SPEED;
            }
            else if (jo_is_pad1_key_pressed(JO_KEY_RIGHT))
            {
                cadet.desiredRot = setRot(desiredCamRot + 90);
                cadet.pos.x += jo_cos(cam_rot.ry) * CADET_SPEED;
                cadet.pos.z += jo_sin(cam_rot.ry) * CADET_SPEED;
            }
        }
        if (jo_is_pad1_key_pressed(JO_KEY_A))
        {
            if (cadet.on_ground && cadet_state != JUMP && cadet_state != FALL)
            {
                if (cadet.velocity.y <= 0)
                {
                    cadet.pos.y -= JO_MULT_BY_65536(CADET_JUMP_SPEED);
                    cadet.velocity.y = JO_MULT_BY_65536(CADET_JUMP_SPEED) * 5;
                    cadet.on_ground = false;
                    cadet_state = JUMP;
                }
            }
        }
        else if (JO_ABS(cadet.cycle) > 0)
        {
            if (cadet.cycle > 0)
            {
                cadet.cycle -= 20;
                //cadet.rot.rz = jo_sin(cadet.cycle / 500);
            }
            else if (cadet.cycle < 0) cadet.cycle += 20;
        }

        if (cadet.prevPos.y < 0) set_cam_target(-cadet.prevPos.x - jo_cos(cam_rot.ry), -cadet.prevPos.y + JO_MULT_BY_65536(50), -cadet.prevPos.z + jo_cos(cam_rot.ry) * 150);
        else set_cam_target(-cadet.prevPos.x - jo_cos(cam_rot.ry),JO_MULT_BY_65536(50), -cadet.prevPos.z + jo_cos(cam_rot.ry) * 150);

        // if (pos.y <= 0)
        // {
        //     velocity.y = 0;
        //     cadet_state = IDLE;
        // }
    }
}

void    create_cadet()
{
    load_cadet_mesh_textures();
    cadet.currentlySelected = true;
    cadet.pos.y = -JO_MULT_BY_65536(200); 
    cadet.col.x = JO_MULT_BY_65536(10);
    cadet.col.y = JO_MULT_BY_65536(10);
    cadet.col.z = JO_MULT_BY_65536(10);
}

void draw_cadet()
{
    jo_3d_push_matrix();

    jo_3d_translate_matrix_fixed(cadet.pos.x, cadet.pos.y, cadet.pos.z);
    
    jo_3d_push_matrix();
    jo_3d_translate_matrix_y(100);
    jo_3d_rotate_matrix_x(180);
    jo_3d_rotate_matrix(cadet.rot.rx, cadet.rot.ry, cadet.rot.rz);
    jo_3d_translate_matrix_fixed(0, (jo_sin(cadet.cycle) * 5), 0);
    display_cadet_mesh();
    jo_3d_pop_matrix();

    jo_3d_pop_matrix();
}