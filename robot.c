#include <jo/jo.h>
#include "robot.h"
#include "fox/foxgfx.h"
#include "physics.h"
#include "camera.h"
#include "level.h"

Character robot;
State robot_state;
int numberOfLoadedMeshes = 1;
PFXDATA robotMesh;

void robot_physics()
{
    jo_vector_fixed result;

    if (robot.on_ground)
    {
        if (robot_state != WALK) robot_state = IDLE;
    }
    else if (robot.velocity.y > -JO_MULT_BY_65536(ROBOT_FALL_SPEED))
    {
        robot.velocity.y -= JO_MULT_BY_65536(GRAVITY);
    }
    //robot.prevPos.y = robot.pos.y;
    robot.pos.y -= robot.velocity.y;
    
    // Check whether our movement vector is passing boundry of AABB
    if (SweptCollisionPointToAABB(&robot.prevPos, &robot.pos, &robot.col, &groundPos, &groundCol, &result))
    {
        // Swept collision response
        robot.pos.x = robot.prevPos.x + jo_fixed_mult(robot.pos.x - robot.prevPos.x, result.x);
        robot.pos.z = robot.prevPos.z + jo_fixed_mult(robot.pos.z - robot.prevPos.z, result.z);
        robot.pos.y = robot.prevPos.y + jo_fixed_mult(robot.pos.y - robot.prevPos.y, result.y);
    }

    for (int c = 0; c < NUM_TILES; c++)
    {
        if (&tile[c].active)
        {
            if (SweptCollisionPointToAABB(&robot.prevPos, &robot.pos, &robot.col, &tile[c].pos, &tile[c].col, &result))
            {
                robot.pos.x = robot.prevPos.x + jo_fixed_mult(robot.pos.x - robot.prevPos.x, result.x);
                robot.pos.z = robot.prevPos.z + jo_fixed_mult(robot.pos.z - robot.prevPos.z, result.z);
                robot.pos.y = robot.prevPos.y + jo_fixed_mult(robot.pos.y - robot.prevPos.y, result.y);
                //jo_printf(0, 26, "PINGAS!");
            }
        }
    }

    robot.on_ground = CollisionCheckY(&robot.prevPos, &robot.pos, &robot.col, &groundPos, &groundCol, &result);

    robot.prevPos = robot.pos;
}

void update_robot()
{
    robot_physics();
    if (robot.currentlySelected)
    {
        if (jo_is_pad1_key_pressed(JO_KEY_UP) || jo_is_pad1_key_pressed(JO_KEY_DOWN) || jo_is_pad1_key_pressed(JO_KEY_LEFT) || jo_is_pad1_key_pressed(JO_KEY_RIGHT))
        {
            if (robot.on_ground)
            {
                robot_state = WALK;
                //robot.cycle -= 5;
            }

            if (robot.rot.ry < robot.desiredRot) robot.rot.ry +=  ROBOT_TURN_SPEED;
            else if (robot.rot.ry > robot.desiredRot) robot.rot.ry -=  ROBOT_TURN_SPEED;

            // Check whether our movement vector is passing boundry of AABB
            if (jo_is_pad1_key_pressed(JO_KEY_UP))
            {
                robot.desiredRot = setRot(desiredCamRot + 180);
                robot.pos.x -= jo_sin(cam_rot.ry) * ROBOT_SPEED;
                robot.pos.z += jo_cos(cam_rot.ry) * ROBOT_SPEED;
            }
            else if (jo_is_pad1_key_pressed(JO_KEY_DOWN))
            {
                robot.desiredRot = setRot(desiredCamRot);
                robot.pos.x += jo_sin(cam_rot.ry) * ROBOT_SPEED;
                robot.pos.z -= jo_cos(cam_rot.ry) * ROBOT_SPEED;
            }

            if (robot.cycle >= 180) robot.cycle = 0;
            else if (robot.cycle <= 0) robot.cycle = 180;
            
            if (jo_is_pad1_key_pressed(JO_KEY_LEFT))
            {
                robot.desiredRot = setRot(desiredCamRot + 90);
                robot.pos.x -= jo_cos(cam_rot.ry) * ROBOT_SPEED;
                robot.pos.z -= jo_sin(cam_rot.ry) * ROBOT_SPEED;
            }
            else if (jo_is_pad1_key_pressed(JO_KEY_RIGHT))
            {
                robot.desiredRot = setRot(desiredCamRot - 90);
                robot.pos.x += jo_cos(cam_rot.ry) * ROBOT_SPEED;
                robot.pos.z += jo_sin(cam_rot.ry) * ROBOT_SPEED;
            }
        }

        if (robot.prevPos.y > 0) set_cam_target(-robot.prevPos.x - jo_cos(cam_rot.ry), cam_pos.y = robot.prevPos.y + JO_MULT_BY_65536(100), -robot.prevPos.z + jo_cos(cam_rot.ry) * 150);
        else set_cam_target(-robot.prevPos.x - jo_cos(cam_rot.ry),JO_MULT_BY_65536(150), -robot.prevPos.z + jo_cos(cam_rot.ry) * 150);

        //robot.prevPos = robot.pos;
        
        // if (pos.y <= 0)
        // {
        //     velocity.y = 0;
        //     robot_state = IDLE;
        // }
    }
}

void    create_robot()
{
    LoadMeshFromSSV("ROBOT.SSV",&robotMesh,3);
    jo_sprite_add_tga(JO_ROOT_DIR, "PANEL.TGA", JO_COLOR_White);
    jo_sprite_add_tga(JO_ROOT_DIR, "GRAY.TGA", JO_COLOR_Gray);
    jo_sprite_add_tga(JO_ROOT_DIR, "RSCREW.TGA", JO_COLOR_Gray);
    jo_sprite_add_tga(JO_ROOT_DIR, "ARM.TGA", JO_COLOR_White);
    jo_sprite_add_tga(JO_ROOT_DIR, "MOVPLAT2.TGA", JO_COLOR_White);
    robot.currentlySelected = false;
    //robot.pos = tile_pos;
    robot.pos.y = -JO_MULT_BY_65536(350);
    robot.pos.x = -JO_MULT_BY_65536(200); 
    robot.pos.z = -JO_MULT_BY_65536(200);
    robot.col.x = JO_MULT_BY_65536(50);
    robot.col.y = JO_MULT_BY_65536(10);
    robot.col.z = JO_MULT_BY_65536(50);
    robot.prevPos = robot.pos;
    robot.on_ground = false;
}

void draw_robot()
{
    jo_3d_push_matrix();

    jo_3d_translate_matrix_fixed(robot.pos.x, robot.pos.y, robot.pos.z);
    
    jo_3d_push_matrix();
    jo_3d_rotate_matrix_x(180);
    jo_3d_rotate_matrix(robot.rot.rx, robot.rot.ry, robot.rot.rz);
    //jo_3d_translate_matrix_x(50);
    //jo_3d_translate_matrix_y(-10);
    jo_3d_set_scale(4, 4, 4);
    //jo_3d_translate_matrix_fixed(0, -JO_MULT_BY_65536(30), 0);
    FXDrawMeshStatic(&robotMesh,CL_Replace);
    //jo_3d_draw(loaded);
    
    jo_3d_pop_matrix();

    jo_3d_pop_matrix();
}