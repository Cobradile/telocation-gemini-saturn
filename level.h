#pragma once
#define TILE_SIZE (120)
#define CUBE_COL_XZ (90)
#define CUBE_COL_Y (155)
#define CUBE_POS_Y (60)
#define WALL_COL_Y (210)
#define WALL_POS_Y (120)
#define NUM_TILES (300)
#define CULL_DISTANCE (130)

typedef enum
{
    FLOOR,
    CUBE,
    WALL
} ObjectType;

typedef struct
{
    ObjectType type;
    bool active;
    jo_pos3D_fixed pos, col;
    jo_rot3D rot;
} LevelElement;

// jo_3d_mesh * loadLevel(const char *file, const char *dir);
extern jo_pos3D_fixed      groundPos, groundCol, cullCol;
extern LevelElement        tile[NUM_TILES];

void initialise_level(void);
void create_tile(ObjectType object, int nx, int nz);
void create_plane(void);
void create_ghost(void);
void draw_ghost(void);
void draw_floor(void);
void draw_tiles(void);